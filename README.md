# What is this?:
- This is a code to search protein/protein interaction networks  
- See also documentation at [link](https://bending456.github.io/Macrophage)

# Installation:
```
apt-get install graphviz libgraphviz-dev pkg-config
pip3 install networkx
pip3 install pygraphviz
pip3 install graphviz
```

# To run:
```
python3 pathwaySearch.py -run -network <x.sif>
python pathwaySearch.py -network networkExtended.sif -receptorName CCR1 -mRNAName mRNAseq_all.csv  -run 
```

## Mansuy-Aubert DMM paper 
```
python3 pathwaySearch.py -mansuyaubert
```
Can also run and render pathway using render_path.ipynb

## Richards work (first paper)       
```
python3 pathwaySearch.py -richards       
```

## Richard's work (second paper) 
```
python3 pathwaySearch.py -richards2      
```
See also sgk1_demo.ipynb


# Software development 
## Unit test:
```
python pathwaySearch.py -unittest
```

## Version control:
- Please refer to [link](https://wiki.luc.edu/pages/viewpage.action?pageId=239797527) for guidance on version control (git/bitbucket) for this code. 
- <b>All committed codes must pass the UnitTest routine defined below before pushing!</b>

## Issues: 
- Jira issues page is provided [here](https://pete-kekenes-huskey.atlassian.net/jira/software/projects/MAC/boards/1)


## Todo: 
- remove duplicate entries from sif file
- check for duplicates that differ only by upper/lower case

## To add:
- p38 activated by (p2x4, 7?) 
- p38 + nfkb --> TNF mRNA transcription https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4099309/
Still not clear if p2x4 significantly promotes nfkb or if p2x7 must be involved 


# Files: 
Contains four columns
1. gene/node name
2. up/downregulation
3. target
4. isReceptor 

## ligandlist.yaml 
- list of ligands for each receptor
GENE1: [GENE2]
where GENE2 is the ligand of GENE1


## receptorlist.txt
- list of nodes that are receptors 
GENE1


## networkExtended.sif 
- more detailed network file than network.sif
GENE1	up-regulates	GENE2

## Chris_mAb.csv
- list of fold-changes in expression as measured by ratio of mAb for M2/M1ev
- derived from ArrayData-UKY-Neupane-200928.xls 
- M1evs is S1, M2 is S2, 


## Ab_Chris.csv
- Same as Chris_mAb????

## phospho_Chris.csv
- Same as Chris_mAb, but using phosphorylated mAb
- also contains names for proteins in network? 

# Other resources:
- [hdacs](https://docs.google.com/document/d/1KgDi9sGg_EY2-jeL7xzWl8xoMUGC15Qy7CTvVltsbgI/edit)
- [p2y pathways](https://docs.google.com/spreadsheets/d/1x-3Pc6qZSoutVSeVXL2ztx4ZiO7yIdcFQvGyzVkx8qs/edit#gid=516122516)
- [polarization](https://docs.google.com/spreadsheets/d/1RSa8unPXNCMmHO-jYktPXmvfbm6KJXwh54Aa_EfY4Tk/edit#gid=741211765)
