import pandas as pd 
import numpy as np

def dataSorter():
    ## Common secretory dataset from SubCell BarCode
    rawdata = open('database/secretory.csv','r')
    data = {}
    counter = 0 
    for line in rawdata: 
        if line.strip():
            if counter == 0:
              counter = counter + 1 
              line = line.strip("\n' '")
              data_name = line.split(",")
              for i,j in enumerate(data_name):
                  if i == 1 or i == 2 or i == 6 or i == 7 or i == 8 or i == 9:             # Can we simplify this? 
                    A = j.translate({ord(n): None for n in '"'})
                    A = A.translate({ord(n): None for n in '\n'})                            # dropping quotation symbols - I don't know why they come along anyway
                    data[A]=[]
            else:
              line = line.strip("\n' '")
              line = line.split(",")
              key_list = list(data.keys())                                               # This looks awfully wastig memory 
              counter2 = 0
              for i,j in enumerate(line):
                  if i == 1 or i == 2 or i == 6 or i == 7 or i == 8 or i == 9:
                    B = j.translate({ord(n): None for n in '"'})
                    data[key_list[counter2]].append(B)
                    counter2 += 1
    rawdata.close()
    # Checking the data
    common_secretory = data


    ## Unique for U251
    rawdata = open('database/uniq.U251.csv','r')
    data = {}
    counter = 0 
    for line in rawdata: 
        if line.strip():
            if counter == 0:
              counter += 1 
              line = line.strip("\n' '")
              data_name = line.split(",")
              for i,j in enumerate(data_name):
                  if i != 0:             # Can we simplify this? 
                    A = j.translate({ord(n): None for n in '"'})
                    A = A.translate({ord(n): None for n in '\n'})                            # dropping quotation symbols - I don't know why they come along anyway
                    data[A]=[]
            else:
                line = line.strip("\n' '")
                line = line.split(",")
                key_list = list(data.keys())                                               # This looks awfully wastig memory 
                counter2 = 0
                for i,j in enumerate(line):
                    if i != 0:
                      B = j.translate({ord(n): None for n in '"'})
                      data[key_list[counter2]].append(B)
                      counter2 += 1


    ## Processing the data sorted by Neighborhood.Class
    Neighborhood_class = data['Neighborhood.Class']
    processed_data = {}
    for key in ['Protein','Secretory','S1','S2','S3','S4','cell']:
        processed_data[key] = []

    key_list = list(processed_data.keys())
    for i,j in enumerate(Neighborhood_class):
        if j == 'Secretory':
            for key in key_list:
                processed_data[key].append(data[key][i])

    rawdata.close()
    U251_secretory_uniq = processed_data

    # the list of ER proteins from UniProt database
    rawdata = open('database/ER.txt','r')

    data = {}
    counter = 0

    for line in rawdata:
        if line.strip():
            line = line.strip("\n' '")
            line = line.split("	")
    
            if counter == 0:
              counter += 1
              for name in line:
                  data[name] = []
            else:
                name_list = list(data.keys())
                for i,j in enumerate(line):
                    data[name_list[i]].append(j)

#df = pd.DataFrame(data)
#df
    data_mem = []
    data_ER = []
    for i,j in enumerate(data['GO NAME']):
        if j == 'endoplasmic reticulum membrane':
            data_mem.append(data['SYMBOL'][i])
        else:
            data_ER.append(data['SYMBOL'][i])

    data_mem_refined = np.unique(data_mem)
    data_ER_refined = np.unique(data_ER)
    common_ER = set(data_mem_refined).intersection(data_ER_refined)

    # Secretory in the list of unique proteins throughout the U251 available in the SubCell database 
    rawdata = open('database/gene_result_NCBI.csv','r')
    data = {}
    counter = 0 
    for line in rawdata: 
        if line.strip():
            if counter == 0:
                counter = counter + 1 
                line = line.strip("\n' '")
                data_name = line.split(",")
                for i,j in enumerate(data_name):
                    data[j]=[]

            else:
                line = line.strip("\n' '")
                line = line.split(",")
                key_list = list(data.keys()) 
                counter2 = 0
                for i in np.arange(len(key_list)):
                    data[key_list[i]].append(line[i])
                    counter2 += 1

    receptorlist = data

    # Secretory in the list of unique proteins throughout the U251 available in the SubCell database 
    rawdata = open('database/inflamm_related_receptor_NCBI.csv','r')
    data = {}
    counter = 0 
    for line in rawdata: 
        if line.strip():
            if counter == 0:
                counter = counter + 1 
                line = line.strip("\n' '")
                data_name = line.split(",")
                for i,j in enumerate(data_name):
                    data[j]=[]

            else:
                line = line.strip("\n' '")
                line = line.split(",")
                key_list = list(data.keys()) 
                counter2 = 0
                for i in np.arange(len(key_list)):
                    data[key_list[i]].append(line[i])
                    counter2 += 1

    inflammreceptorlist = data

    return common_secretory, U251_secretory_uniq, data_mem_refined, data_ER_refined, common_ER, receptorlist, inflammreceptorlist

def dataextract(filename):
    
    rawdata = open(filename,'r')
    data = {}
    counter = 0 
    for line in rawdata: 
        if line.strip():
            if counter == 0:
                counter = counter + 1 
                line = line.strip("\n' '")
                data_name = line.split(",")
                for i,j in enumerate(data_name):
                    data[j]=[]

            else:
                line = line.strip("\n' '")
                line = line.split(",")
                key_list = list(data.keys()) 
                counter2 = 0
                for i in np.arange(len(key_list)):
                    data[key_list[i]].append(line[i])
                    counter2 += 1

    return data