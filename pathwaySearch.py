import networkx as nx
import numpy as np
#import matplotlib.pyplot as plt
#from networkx.drawing.nx_agraph import graphviz_layout
#from graphviz import Digraph
import utilities as util
import yaml 
import pandas as pd 
import re 

projectPath = "./projects/kolesar/"
networkPath = "./networks/"

### REFACTOR INTO UTILITIES
def GetDetails(node, expData): # data_mrna,data_prot1,prot_name1,data_prot2,prot_name2):
    """
    Storing 'useful' information about each node (mrna, mAb, p-mAb)
    """
    stored={}
    prot_exp1, M1, M2, M1M2, logFCofM1M2overM2 = util.expression_search(node,
                                                                       expData['data_mrna'],
                                                                       expData['data_prot1'],
                                                                       expData['prot_name1'])

    if prot_exp1 == 'N/A':
      stored['mAb(From M2 to M1: M1/M2)'] = 'N/A' 
    else:
      stored['mAb(From M2 to M1: M1/M2)'] = 1/float(prot_exp1)

    prot_exp2, M1, M2, M1M2, logFCofM1M2overM2 = util.expression_search(node,
                                                                       expData['data_mrna'],
                                                                       expData['data_prot2'],
                                                                       expData['prot_name2'])

    if prot_exp2 == 'N/A':
      stored['Phospho(From M2 to M1: M1/M2)'] = 'N/A' 
    else:
      stored['Phospho(From M2 to M1: M1/M2)'] = 1/float(prot_exp2)

    stored['M1_mRNA'] = M1
    stored['M2_mRNA'] = M2
    stored['M1M2_mRNA'] = M1M2
    stored['logFC(M1M2/M2)'] = logFCofM1M2overM2   
    return stored 

def CalcCycles(G,receptor,edge, orientation = "original", verbose=False):
      """
      Looks for negative/positive feedback inhibition loops in network G

      Parameters
      ----------
      G : Networkx graph object
          DESCRIPTION.
      orientation: string    
          original/ignore
      Returns
      -------
      None.

      """
      print("Looking for cycles containing receptor ",receptor, " and edge ", edge)
      trialPath = nx.dijkstra_path(G,edge,"M1_polarization")       

      # ------------- Cycle Code -----------------#
      Original = list(nx.find_cycle(G,orientation=orientation)    )
      Node_original = []
      
      if orientation=="ignore":
          cycleType = "undirected"
      else:
          cycleType="directed"
      
      if verbose: 
          print('')
          print('------------%s-----------'%cycleType)
      for entry in Original:
        start_node = entry[0]
        end_node = entry[1]
        score = G.get_edge_data(start_node,end_node)['weight']
        if verbose: 
            if score > 10:
              feature = 'Inhibitory'
              print(start_node,'---|',end_node,' with weight of ',score,': ',feature)  
            else:
              feature = 'Promoting'
              print(start_node,'--->',end_node,' with weight of ',score,': ',feature)
        Node_original.append(start_node)
        Node_original.append(end_node)
    
      Node_original_unique = np.unique(Node_original)
      
      if verbose: 
          print('')
          print('---- Check if node of interest appears in the cycle ---')
      for tnode in trialPath:
        #print("x",tnode)
        for pair in Original:
          checker = list(filter(lambda d: tnode in d, pair))
          if checker:
            print(tnode, "is in a ", cycleType, " cycle  at edge ", list(pair))
            
 
      return    
### REFACTOR INTO UTILITIES

#
# runItAll function 
#
print("by default these first three should not be defined") 
def runItAll(
              mRNA_inputfilename = projectPath+'mRNAdata', 
              Prot_inputfilename1 = projectPath+'Ab_Chris_refined', # contains gene name 
#              Prot_inputfilename1 = 'Ab_Chris',
              #Prot_inputfilename2 = 'phospho_Chris',
              Prot_inputfilename2 = projectPath+'phospho_Chris_refined',
              network_inputfilename = networkPath+ 'network',
              receptor_list = 'receptorlist',
              ligand_list_file_name = 'ligandlist',
              destination = 'M1_polarization',
              unit_test_receptor = False,
              display=True,
              listofNodes_path=True,
              ): 
  """
  This function collects all pathways (I think) linking receptors to a destination (like M1)
  Defining 'target' and 'control' cases, but I don't know what that means'
  Table of datais generated 
  """
  ## Minor
  

  ##
  ## Misc
  ## 
  
  #unique_all, networkdata, edge_mRNA_score_M2toM12, edge_mRNA_score_M12toM2, edge_mRNA_score_M2toM1, edge_mRNA_score_M1toM2 = 
  networkData =   util.prepare_process(network_inputfilename,mRNA_inputfilename)
  unique_all = networkData['unique_all']
  networkdata = networkData['networkdata']
  
  #print(len(networkData['edge_mRNA_score_M2toM12']))

      
  if destination == 'pro-inflammatory' or destination == 'M1_polarization':
    # target???? 
    # M2/M12
    try:
        edge_mRNA_score_M2toM12 = networkData['edge_mRNA_score_M2toM12']
        # using M2/M12 mRNA data  (as target)
        collected_data1, PathwayNodes1 = util.networkAnalaysis(
            unique_all, networkdata, 'new_figure_target'+destination, 
            receptor_list, destination,listofNodes_path,edgeScores=edge_mRNA_score_M2toM12, display=False)    
    except:
        print("Using default edges")
        edgeScore = networkData['default']

        # using M2/M12 mRNA data  (as target)
        collected_data1, PathwayNodes1 = util.networkAnalaysis(
            unique_all, networkdata, 'new_figure_target'+destination, 
            receptor_list, destination,listofNodes_path,edgeScores=edgeScore, display=False)
  
    # control??
    # M2/M1  
    try:
        edge_mRNA_score_M2toM1 = networkData['edge_mRNA_score_M2toM1']
        collected_data2, PathwayNodes2 = util.networkAnalaysis(
            unique_all, networkdata, 'new_figure_control'+destination, 
            receptor_list, destination,listofNodes_path,edgeScores=edge_mRNA_score_M2toM1,display=False)
    except:
        edgeScore = networkData['default']
        # using M2/M12 mRNA data  (as target)
        collected_data2, PathwayNodes2 = util.networkAnalaysis(
            unique_all, networkdata, 'new_figure_control'+destination, 
            receptor_list, destination,listofNodes_path,edgeScores=edgeScore, display=False)
    
    collected_data1a, PathwayNodes1a = util.networkAnalaysis(
        unique_all, networkdata, 'new_figure_unweight'+destination, 
        receptor_list, destination,listofNodes_path,edgeScores=None,display=False)   
    

  elif destination == 'anti-inflammatory' or destination == 'M2_polarization':
      
    try:
        edge_mRNA_score_M2toM12 = networkData['edge_mRNA_score_M12toM2']
        # using M2/M12 mRNA data  (as target)
        collected_data1, PathwayNodes1 = util.networkAnalaysis(
            unique_all, networkdata, 'new_figure_target'+destination, 
            receptor_list, destination,listofNodes_path,edgeScores=edge_mRNA_score_M2toM12, display=False)    
    except:
        print("Using default edges")
        edgeScore = networkData['default']

        # using M2/M12 mRNA data  (as target)
        collected_data1, PathwayNodes1 = util.networkAnalaysis(
            unique_all, networkdata, 'new_figure_target'+destination, 
            receptor_list, destination,listofNodes_path,edgeScores=edgeScore, display=False)
  
    # control??
    # M2/M1  
    try:
        edge_mRNA_score_M2toM1 = networkData['edge_mRNA_score_M1toM2']
        collected_data2, PathwayNodes2 = util.networkAnalaysis(
            unique_all, networkdata, 'new_figure_control'+destination, 
            receptor_list, destination,listofNodes_path,edgeScores=edge_mRNA_score_M2toM1,display=False)
    except:
        edgeScore = networkData['default']
        # using M2/M12 mRNA data  (as target)
        collected_data2, PathwayNodes2 = util.networkAnalaysis(
            unique_all, networkdata, 'new_figure_control'+destination, 
            receptor_list, destination,listofNodes_path,edgeScores=edgeScore, display=False)
    
    collected_data1a, PathwayNodes1a = util.networkAnalaysis(
        unique_all, networkdata, 'new_figure_unweight'+destination, 
        receptor_list, destination,listofNodes_path,edgeScores=None,display=False)


      

  # PKH, awful way to hack this out
  G = collected_data1['g'] 
  collected_data1.pop('g')
  collected_data1a.pop('g')
  collected_data2.pop('g')
  #collected_data2a.pop('g')

  df_target  = pd.DataFrame(collected_data1)
  df_target_unweighted = pd.DataFrame(collected_data1a)
  df_control = pd.DataFrame(collected_data2)
  #df_control_unweighted = pd.DataFrame(collected_data2a)

  if 'sif' not in network_inputfilename:
    network_inputfilename = re.sub('\.sif','',network_inputfilename)
  #print(network_inputfilename ); quit;
  

  #####################################################
  ### Extracting Gene names from mRNA sequence data ###
  #####################################################
  expData = dict()
  if mRNA_inputfilename is not None:
    mRNA_inputfilename = re.sub('\.csv','',mRNA_inputfilename)
    expData['data_mrna'] = util.reader(mRNA_inputfilename)
  else:
    expData['data_mrna'] =None   
      
  #########################################
  ### Extracting protein names and data ###
  #########################################
  if Prot_inputfilename1 is not None:
    expData['data_prot1'], expData['prot_name1'] = util.protReader(Prot_inputfilename1)
  else:
    expData['data_prot1'], expData['prot_name1'] = None, None
    
  if Prot_inputfilename2 is not None:
    expData['data_prot2'], expData['prot_name2'] = util.protReader(Prot_inputfilename2)
  else:
    expData['data_prot2'], expData['prot_name2'] = None, None
    
  ########################################################################
  ### Extracting Node names from pathway file generated from Cytoscape ###
  ########################################################################
  data_network = util.reader(network_inputfilename,sif=True) 

  #########################################################
  ### Extracting Receptor node names from Receptor List ###
  #########################################################
  receptor_list = util.textreader(receptor_list) 

  ##############################################################################
  ### Sorting nodes and edges                                                ###
  ###           and add the weight to each edge based on its unique feature  ###
  ##############################################################################
  node_names = np.unique(data_network['start_nodes']+data_network['end_nodes']) ## get unique node name

  with open(ligand_list_file_name+'.yaml') as file:
    ligand_list = yaml.load(file, Loader=yaml.BaseLoader)

  receptor_specifics = {}
  ligand_specifics = {}
  
  


        

  # collecting mrna, expression data for each R and its pathway members
  # do for nodes (downfield of receptors)
  for R in receptor_list['Receptors']:
    receptor_specifics[R] = {}
    ligand_specifics[R] = {}

    try:
      list_node = PathwayNodes1[R]
      for node in list_node:
        #receptor_specifics[R][node] = {}
        receptor_specifics[R][node] = GetDetails(node, expData) # data_mrna,data_prot1,prot_name1,data_prot2,prot_name2) 
       
    except:
      pass

    # do for ligands (upfield of receptors)
    try: 
      list_ligand = ligand_list[R]    
      for ligand in list_ligand:
        ligand_specifics[R][ligand] = GetDetails(node, expData) #data_mrna,data_prot1,prot_name1,data_prot2,prot_name2)


    except:
      pass

  # PKH add the graph back
  results = {}
  results['G'] = G
  results['M1target']= collected_data1
  results['M1control']= collected_data2
  results['M1unweighted']= collected_data1a
  results['M1targetPath']= PathwayNodes1
  results['M1controlPath']= PathwayNodes2
  results['M1unweightedPath']= PathwayNodes1a,
  results['M1target_df']= df_target
  results['M1contro_df']= df_control
  results['M1target_df_un']= df_target_unweighted
  results['path_M1']= receptor_specifics
  results['ligand_expM1']= ligand_specifics


  return results



#
# Placeholder for unit test 
#
networkName = "network.sif"
mRNAName = "mRNAdata.csv"
Receptor_Name = 'IL1R1'
def UnitTest() :
    
    ## Richards test 
    #dfs['Receptor']
    dfs = Richards()
    # get IL1R1 row 
    testRow = dfs.loc[dfs['Receptor']=="IL1R1" ]
    testScore = testRow[["M1_target/M2_control"]].values
    prevValue =  0.53881537
    testDiff = np.abs ( testScore[0] - prevValue )
    assert testDiff < 1e-5, "FAIL"
    print("PASS")
    
    
    ## Mansuy-Aubert test
    results = MansuyAubert()
    inflPath = results['inflPath'] 
    # simple unit test
    # last inflPath should contain NLRP3 as 1-index entry
    assert (inflPath[1] == "NLRP3"),"Code is not returning previous result"
    print("PASS!")
    


#
# Goal is to restrict search to PM proteins and look for those culminating in M2 functions or M1 functions
# May need for Geraldine to annotate our existing node list based on localization data (did she already to this?) 
# BC, GSR, and PKH 
#
def Richards():
  """
  TODO    
    search over PM proteins
      use mrna data to weight m1/m2 etc
      use mab data to confirm mrna
      which pathways promote M1
      rank by scores
      canditates for Ag 

      which pathways promote M2
      candidates for KO 

    repeat for ER 
  """
  
    #[M1target, M1control, M1unweighted, M1targetPath, M1controlPath, M1unweightedPath, M1target_df, 
    #M1contro_df, M1target_df_un, M1control_df_un, path_M1, ligand_expM1]  =\
  # PRO      
  resultsM1 = runItAll(
    mRNA_inputfilename = projectPath + 'mRNAseq_all', 
    Prot_inputfilename1 = projectPath + 'Ab_Chris_refined',
    Prot_inputfilename2 = projectPath + 'phospho_Chris_refined',
    network_inputfilename = networkPath + 'networkExtended-looped4',
    receptor_list = 'receptorlist',
    ligand_list_file_name = 'ligandlist',
    destination = 'pro-inflammatory',
    unit_test_receptor = 'IL1R1', # BC: what is this for? 
    listofNodes_path = False)

# [M2target, M2control, M2unweighted, M2targetPath, M2controlPath, M2unweightedPath, M2target_df, 
#  M2contro_df, M1target_df_un, M1control_df_un, path_M2, ligand_expM2]  = 
  resultsM2 = runItAll(
    mRNA_inputfilename = projectPath + 'mRNAseq_all', 
    Prot_inputfilename1 = projectPath + 'Ab_Chris_refined',
    Prot_inputfilename2 = projectPath + 'phospho_Chris_refined',
    network_inputfilename = networkPath + 'networkExtended-looped4',
    receptor_list = 'receptorlist',
    ligand_list_file_name = 'ligandlist',
    destination = 'anti-inflammatory',
    unit_test_receptor = 'IL1R1',
    listofNodes_path = False)

  # compute results 
  M1target = resultsM1['M1target']
  print("Need to replace M1target w target")
  M1control = resultsM1['M1control']
  print("PLACEHOLDER FOR NOW ")
  M2target = resultsM2['M1target']
  M2control = resultsM2['M1control']
  M1M2totalScore = util.TableMaker(M1target, M2target, M1control, M2control)
  
  # rank and report 
  df = pd.DataFrame(M1M2totalScore)
  #pd.set_option('display.max_rows', None)
  #pd.set_option('display.max_columns', None)
  #pd.set_option('display.width', None)
  #pd.set_option('display.max_colwidth', -1)
  
  dfs = df.sort_values('M1_target/M2_control',ascending=True)
  print( dfs ) 
  dfs.to_csv("out.csv")
  
  Receptor_Name = 'TNFRSF1A'
  path_M1 = resultsM1['path_M1']
  receptorDF= pd.DataFrame(path_M1[Receptor_Name])
  print(receptorDF)
  
  
  
  print("TODO: add unit test")
  return dfs

#
# New tests 
# THis test searches for all nodes that use butyrate as a ligand
# For each receptor, it determines whether M1 is inhibited and by what
# Ben Chun and PKH 
#
def MansuyAubert(
   ) :
  ## identify receptors (for now) that are sensitive to the input ligand 
  ligand = "butyrate"
  moleculeList = "moleculeslist.yaml" 
  with open( moleculeList ) as file:
    molecule_list = yaml.load(file, Loader=yaml.BaseLoader)
  # for all receptors, find 

  #print(molecule_list)
  ligandSensitiveTargets = []
  for key,values in molecule_list.items():
      #print(key, values)
      if ligand in values: ligandSensitiveTargets.append( key ) 

  print("The following are sensitive to %s"%ligand, ligandSensitiveTargets)



  ## initialize graph by finding the SHORTEST path from each receptor to the M1 state
  networkName = "networks/networkExtended.sif" 
  results = runItAll(network_inputfilename=networkName,
              listofNodes_path = False, # flag for printing  
              mRNA_inputfilename = None,#'mRNAdata', 
              Prot_inputfilename1 = None,#'Ab_Chris_refined', # contains gene name 
              Prot_inputfilename2 = None,#'phospho_Chris_refined',
                   )
  
  # get G (the graph) from M1target
  G = results['G'] 

  import networkx as nx

  ## Get FFAR2-mediated paths 
  # for each receptor sensitive to ligand, find inhibitory paths from each of its neighbors
  # DMMfix
  #print("NEED TO DIFFERENTIATE ANTAG/AG; HDAC promotes M1 so inhib suppress ")
  inhibitoryReceptors=[]
  inhibitoryPaths=[]
  for receptor in ligandSensitiveTargets: 
    print(receptor)
    # find each edge eminating from target 
    for node,edge in G.out_edges(receptor):
      print(receptor, " --> ", edge)
      # check if path exists 
      try:
        trialPath = nx.dijkstra_path(G,edge,"M1_polarization") 
        #print(trialPath)
        trialPath = [receptor] + trialPath
        
        result = isInhibitory(G,trialPath,receptor) 
        if result: 
           print(receptor, " -> ", trialPath) 
           inhibitoryReceptors.append(receptor)
           inhibitoryPaths.append(trialPath)

      # skip if no path
      except:
        continue

  ffar2Path = inhibitoryPaths[0]   

  # simple unit test
  # last inflPath should contain NLRP3 as 1-index entry
  assert (inhibitoryReceptors[0] == "FFAR2"),"Code is not returning previous result"
  print("PASS!")
  
  ## HDAC3 mediated paths
  # DMMfix  
  for receptor in ligandSensitiveTargets: 
    print(receptor)
  hdac3Path = []
  for trialPath in nx.all_simple_paths(G, "HDAC3", "M1_polarization"):
      if isInhibitory(G,trialPath,"HDAC3"):   
        continue 

      print(trialPath, " promotes inflammation")
      hdac3Path = trialPath

  ## Get TLR4-mediated inflammatory path 
  # We hypothesize that under WD conditions that serum LPS is elevated, which
  # we anticipate would stimulate TLR4. TLR4 is a known promoter of "NfKb-p65/p50"
  # selecting one of the many paths (arbitrarily chose len 5)
  tlr4Path = None      
  for trialPath in nx.all_simple_paths(G, "TLR4", "NfKb-p65/p50"):
      if isInhibitory(G,trialPath,"TLR4"):   
        print("Double check, since sampling multiple paths") 
        continue 
      else:
        1
        #print(trialPath, " promotes NFKB")

      if tlr4Path is None:
        tlr4Path = trialPath
      if len(trialPath)<len(tlr4Path):
        tlr4Path = trialPath


  ## NFKB effectors 
  # print all downfield of NFKB to show what is activated 
  print("Printing all paths mediated by NFKB") 
  inflPath = []
  for trialPath in nx.all_simple_paths(G, "NfKb-p65/p50", "M1_polarization"):
      #print(inflPath)
      if len(trialPath) > len(inflPath): 
        inflPath = trialPath
  print(inflPath) 

  ## we also have recent evidence that HDAC3 inhibits HAHDA,thereby promoting
  # ROS production from mitochondrium, which primes NLRP3

  ## Merge pathways into a subset
  tlr4Path = tlr4Path[:-1] + inflPath 
  print(tlr4Path) 

  ffar2Path = ffar2Path[:-2] + inflPath 
  print(ffar2Path)
  
  
  calcCycles = False
  if calcCycles: 
    CalcCycles(G,receptor,edge,orientation="original")
    CalcCycles(G,receptor,edge,orientation="ignore")
  
  ## store data 
  results['inflPath'] = inflPath
  results['tlr4Path'] = tlr4Path
  results['ffar2Path'] = ffar2Path
  results['hdac3Path'] = hdac3Path
  return results 
 
#
# checks if pathway is overall inhibory or agonistic 
#
def isInhibitory(G,path,receptorName):
  ## for each receptor, find paths 

  # check for inhibitory vs agonistic edges; assign -1 if inhibitory
  # This could change, but right now we assume anything >100 is inhibitory
  # as this is how up/down regulation was handled in the first place 
 # print(path)
  #print("-->",path)
  nEdges = len(path)-1
  edgeID = np.ones ( nEdges ) 
  for i in range(nEdges )           :  
      n1,n2 = path[i],path[i+1]
      attr = G.get_edge_data(n1,n2)
      weight = attr['weight'] 
      #print(n1,n2, weight)
      if weight > 100 : edgeID[i] =-1
  #print(edgeID)

  # check for inhibition 
  daProd = np.prod(edgeID)
  args = np.argwhere(edgeID < 0)
  result = False
  #print("ssdf",edgeID)
  if daProd < 0:
    result = True
    print("%s/%s inhibits %s"%(path[0],path[1], path[-1]))
    print("Inhibition occurs at ")
    for j in args:
        j = int(j)
        print(" ", path[j],"--|",path[j+1])
  else:
    #print(path[0]," does not inhibit ", path[-1])
    1

  return result 


def cycle_check(Receptor_name,
                NetworkData,
                PathwayOnly,
                trail_color,
                cycle_color,
                searching_param,
                output_name):
                
  G = NetworkData['g']
  cycle_path = list(nx.find_cycle(G,orientation=searching_param))
  ReceptorName = Receptor_name
  trail_path = PathwayOnly[ReceptorName]
  
  counter = 0
  for vart in trail_path:
    for pair in cycle_path:
      varc = pair[0]
      if vart == varc:
        print(pair)
        counter += 1

  if counter == 0:
    print("The pathway of interest and cycle pathway do not share a common node")

  from graphviz import Digraph
  Gtrail = Digraph('unix', filename='fullpicture',format='png',
                    node_attr={'shape':'box',
                               'color': trail_color, 
                               #'style': 'filled',
                               'fontcolor':'black'},
                    edge_attr={'color': trail_color}) 
  for nodet in trail_path:
    Gtrail.node(nodet)

  for i,j in enumerate(trail_path):
    if i < len(trail_path)-1:
      attr = G.get_edge_data(j,trail_path[i+1])
      if attr['weight'] > 100 : 
        name = 'inhibitory'
      else:
        name = 'stimulatory'

      Gtrail.edge(j,trail_path[i+1],label=name)
      

  for pair in cycle_path:
    Gtrail.attr('node', shape='box',color=cycle_color)
    Gtrail.node(pair[0])

  for pair in cycle_path:
    node1 = pair[0] 
    node2 = pair[1]
    attr = G.get_edge_data(node1,node2)
    if attr['weight'] > 100:
      name = 'inhibitory'
    else:
      name = 'stimulatory'

    Gtrail.attr('edge',color=cycle_color)
    Gtrail.edge(node1,node2,label=name)

  Gtrail.view(output_name)

  return

  

#!/usr/bin/env python
import sys
##################################
#
# Revisions
#       10.08.10 inception
#
##################################


#
# Message printed when program run without arguments 
#
def helpmsg():
  scriptName= sys.argv[0]
  msg="""
Purpose: 
 
Usage:
"""
  msg+="  %s -run" % (scriptName)
  msg+="""
  
 
Notes:

"""
  return msg

#
# MAIN routine executed when launching this script from command line 
#
if __name__ == "__main__":
  import sys
  msg = helpmsg()
  remap = "none"

  if len(sys.argv) < 2:
      raise RuntimeError(msg)

  #fileIn= sys.argv[1]
  #if(len(sys.argv)==3):
  #  1
  #  #print "arg"

  # Loops over each argument in the command line 
  for i,arg in enumerate(sys.argv):
    # calls 'doit' with the next argument following the argument '-validation'
    if(arg=="-mRNAName"):
        mRNAName= sys.argv[i+1]
    elif(arg=="-network"):
        networkName = sys.argv[i+1]
    elif(arg=="-receptorName"):
        receptorName = sys.argv[i+1]
    elif(arg=="-run"):
         #runItAll()
         runItAll(network_inputfilename=networkName,
             mRNA_inputfilename =mRNAName,
             #receptor_list = receptorName 
                 )
         quit()
    elif(arg=="-unittest"): 
        UnitTest()
        quit()
    elif(arg=="-richards"): 
        Richards()
        # this one should return the same result, but need to compare/merge
        ps2.PathwaySearch(input_dir='projects/richards/',mode='richardsM1')
        quit()    
    elif(arg=="-richards2"): 
        import pathwaySearch2 as ps2
        #input_dir = os.getcwd()  
        DEGs = ['RSAD2', 'PTGS2', 'ELOVL7', 'TAP1', 'SLC8A3', 'IL15RA', 'CYP7B1', 'GRAMD1B', 'TAP2', 'PML', 'PANX1', 'APOL2', 'VRK2', 'INSIG2', 'HLA-C', 'ANTXR2', 'B3GLCT', 'SSR1', 'PTGS1', 'OSBPL3', 'SLC37A2', 'SGK1', 'GRAMD4', 'FLRT2', 'PPM1L']
        ps2.PathwaySearch(input_dir='projects/richards/',mode='richardsM2',
                select_rec = DEGs,
                display=True)
        quit()

    elif(arg=="-mansuyaubert" or arg=="-test1"): 
        MansuyAubert()
        quit()

  #  else:
  #      raise RuntimeError("Argument not understood: "+arg)

  raise RuntimeError("Arguments not understood")
