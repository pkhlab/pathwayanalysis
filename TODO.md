Geraldine's files need to be integrated 

/////
Need to 
- add back the protein mAb data
- define both agonistic and antagonistic ligands (to capture HDAC contributions) 
- add localization as an attribute for the genes 


- .sif files contain network. 
Need to validate pathways used in sif files by adding 4th column with PMID supporting linkage 
Added already for HDAC; however, the PMID appears in cytoscape as a node
Probably need to create a cytoscape-ready file without extraneous columns or use current .sif files as a master for derived network files. 

- differentiate agonists and antagonists in input yaml file 
See 'DMMfix' in pathwaySearch.py 
