import numpy as np
#from networkx.drawing.nx_agraph import graphviz_layout
#
import networkx as nx 
#from IPython.display import Image
import math

################################################################################
def reader(filename,
           sif=False):
    """ SIF file reader """

    if sif == True:

      if 'sif' not in filename:  
          filename = filename+'.sif'
      rawdata = open(filename,'r')
      data = {}
      network_info = ['start_nodes','edge_features','end_nodes']
      counter = 0

      for line in rawdata:
        if line.strip():
          line = line.strip("\n' '")
          line = line.split()
          #line = line.split("	") # ** make it available for both space and tab here

          if counter == 0:
            counter = counter + 1 
            for name in network_info:
              data[name]=[]

          for i in np.arange(len(network_info)):
            data[network_info[i]].append(line[i])

      rawdata.close()

    else:

      if 'csv' not in filename:  
          filename = filename+'.csv'
      rawdata = open(filename,'r')
      data = {}
      counter = 0

      for line in rawdata:
        if line.strip():
          if counter == 0:
            counter = counter + 1 
            line.strip("\n' '")
            data_name = line.split(",")
            # Creating the dictionary data structure
            for name in data_name:
              data[name]=[]
          else:
            line = line.strip("\n' '")
            line = line.split(",")
            # Storing the data in the dictionary 
            for i in np.arange(4):
              element = line[i]
              data[data_name[i]].append(element)

    
      rawdata.close()
    return data

################################################################################
def textreader(filename):
    rawdata = open(filename+'.txt','r')

    receptor = []
    feature = []
    counter = 0

    for line in rawdata:
        if line.strip():
          line = line.strip("\n' '")
          line = line.split(",")
          receptor.append(str(line[0]))
          feature.append(str(line[1]))
    
    data = {'Receptors':receptor, 'Pro/Anti':feature}
    rawdata.close()
    return data

################################################################################
def protReader(filename):
  rawdata = open(filename+'.csv','r')
  data = {}
  name = []
  counter = 0 

  for line in rawdata:
    if line.strip():
      if counter == 0:
        counter = counter + 1
        continue 
      else:
        #print(line)
        line = line.strip("\n' '")
        line = line.split(",")
        # Storing the data in the dictionary 
        if line[1] == "N/A":
          continue
        else:
          data[line[1]] = float(line[2])
          name.append(line[1])

  return data, name

###################################################################################
def prepare_process(networkFileName, 
                    mRNAseqFileName=None, # project-specific mRNA data
                    protFileName = None):
    
    """
    This funtion appears to load both the network file and rna seq data.
    There is some kind of edge reweighting scheme that occurs if rna data are provided
    """
    networkDict = dict()
    
    networkdata = reader(networkFileName, sif=True)
    networkDict['networkdata'] = networkdata
    
    
    num_of_edges = len(networkdata['start_nodes'])
    unique_start = np.unique(networkdata['start_nodes'])
    unique_end = np.unique(networkdata['end_nodes'])
    unique_all = np.unique(networkdata['start_nodes'] + networkdata['end_nodes'])
    networkDict['unique_all'] = unique_all
    
    if mRNAseqFileName:
        rnadata = reader(mRNAseqFileName)
    else: 
        networkDict['default'] = WeightEdges(networkdata['edge_features'])    
        return networkDict
    
    if protFileName:
        protdata, protname = protReader(protFileName) # this will be fixed but for now, nothing will happen 
        raise RuntimeError("where is this being summoned")
  


    print("factor this out ")    

    M2_mRNA_Node = {}
    M2_mRNA_Node_arr = []  
    M1M2_mRNA_Node = {}
    M1M2_mRNA_Node_arr = []
    M1_mRNA_Node = {}
    M1_mRNA_Node_arr = []

    # FC from M2 to M1M2 (specific case for Jill's project )
    logFC_mRNA_Node_M2toM12 = {}
    logFC_mRNA_Node_arr_M2toM12 = []
    # FC from M12 to M2 (specific case for Jill's project )
    logFC_mRNA_Node_M12toM2 = {}
    logFC_mRNA_Node_arr_M12toM2 = []
    # FC from M2 to M1 (M1 polarization from M2, general case)
    logFC_mRNA_Node_M2toM1 = {}
    logFC_mRNA_Node_arr_M2toM1 = []
    # FC from M1 to M2 (M2 polarization from M1, general case)
    logFC_mRNA_Node_M1toM2 = {}
    logFC_mRNA_Node_arr_M1toM2 = []

    for node_name in unique_all:
        i = 0
        for seq in rnadata['mgi_symbol']:
            if node_name.lower() == seq.lower():
                dataM1 =   float(rnadata['M1'][i])
                dataM2 =   float(rnadata['M2'][i])
                dataM1M2 = float(rnadata['M1M2\n'][i])
                
                M2_mRNA_Node[node_name] = dataM2
                M2_mRNA_Node_arr.append(dataM2)
                M1_mRNA_Node[node_name] = dataM1
                M1_mRNA_Node_arr.append(dataM1)
                M1M2_mRNA_Node[node_name] = dataM1M2
                M1M2_mRNA_Node_arr.append(dataM1M2)
                
                # Convert any possible zero to small number
                ## division can screw this up 
                if dataM2 < 1e-14:
                    dataM2 = 1e-14

                if dataM1 < 1e-14:
                    dataM1 = 1e-14

                if dataM1M2 < 1e-14:
                    dataM1M2 = 1e-14

                # FC from M2 to M1M2
                logFCM12M2 = math.log2(dataM1M2/dataM2)
                logFC_mRNA_Node_M2toM12[node_name] = logFCM12M2 
                logFC_mRNA_Node_arr_M2toM12.append(logFCM12M2)
                # FC from M12 to M2
                logFC_mRNA_Node_M12toM2[node_name] = -logFCM12M2
                logFC_mRNA_Node_arr_M12toM2.append(-logFCM12M2)
                # FC from M2 to M1
                logFCM1M2 = math.log2(dataM1/dataM2)
                logFC_mRNA_Node_M2toM1[node_name] = logFCM1M2
                logFC_mRNA_Node_arr_M2toM1.append(logFCM1M2)
                # FC from M1 to M2
                logFC_mRNA_Node_M1toM2[node_name] = -logFCM1M2
                logFC_mRNA_Node_arr_M1toM2.append(-logFCM1M2)

            i += 1 

    ## Convert the mRNA based score on Node 
    print("PKHthese could be refactored ")  
    M2_node_score = {}
    per10M2 = np.percentile(M2_mRNA_Node_arr,10)
    per50M2 = np.percentile(M2_mRNA_Node_arr,50)
    per95M2 = np.percentile(M2_mRNA_Node_arr,95)
    for key, value in M2_mRNA_Node.items():
        raw_value = float(value)
        if raw_value <= per10M2 :
            M2_node_score[key] = 1
        elif raw_value <= per50M2 and raw_value > per10M2:
            M2_node_score[key] = 2
        elif raw_value <= per95M2 and raw_value > per50M2:
            M2_node_score[key] = 3
        elif raw_value > per95M2:
            M2_node_score[key] = 4

    ## Convert the mRNA based score on Node 
    M1_node_score = {}
    per10M1 = np.percentile(M1_mRNA_Node_arr,10)
    per50M1 = np.percentile(M1_mRNA_Node_arr,50)
    per95M1 = np.percentile(M1_mRNA_Node_arr,95)
    for key, value in M1_mRNA_Node.items():
        raw_value = float(value)
        if raw_value <= per10M1 :
            M1_node_score[key] = 1
        elif raw_value <= per50M1 and raw_value > per10M1:
            M1_node_score[key] = 2
        elif raw_value <= per95M1 and raw_value > per50M1:
            M1_node_score[key] = 3
        elif raw_value > per95M1:
            M1_node_score[key] = 4

    ## Convert the mRNA based score on Node 
    M1M2_node_score = {}
    per10M1M2 = np.percentile(M1M2_mRNA_Node_arr,10)
    per50M1M2 = np.percentile(M1M2_mRNA_Node_arr,50)
    per95M1M2 = np.percentile(M1M2_mRNA_Node_arr,95)
    for key, value in M1M2_mRNA_Node.items():
        raw_value = float(value)
        if raw_value <= per10M1M2 :
            M1M2_node_score[key] = 1
        elif raw_value <= per50M1M2 and raw_value > per10M1M2:
            M1M2_node_score[key] = 2
        elif raw_value <= per95M1M2 and raw_value > per50M1M2:
            M1M2_node_score[key] = 3
        elif raw_value > per95M1M2:
            M1M2_node_score[key] = 4

    # Convert the mRNA based score on Node 
    # FC from M2 to M1M2
    FC_node_score_M2toM12 = {}
    for key, value in logFC_mRNA_Node_M2toM12.items():
        raw_value = float(value)
        if raw_value <= 0 :
            FC_node_score_M2toM12[key] = -1 # This is Bad
        else:
            FC_node_score_M2toM12[key] = 1 # This is Good

    # FC from M12 to M2
    FC_node_score_M12toM2 = {}
    for key, value in logFC_mRNA_Node_M12toM2.items():
        raw_value = float(value)
        if raw_value <= 0 :
            FC_node_score_M12toM2[key] = -1 # This is Bad
        else:
            FC_node_score_M12toM2[key] = 1 # This is Good

    # FC from M2 to M1
    FC_node_score_M2toM1 = {}
    for key, value in logFC_mRNA_Node_M2toM1.items():
        raw_value = float(value)
        if raw_value <= 0 :
            FC_node_score_M2toM1[key] = -1 # This is Bad
        else:
            FC_node_score_M2toM1[key] = 1 # This is Good

    # FC from M1 to M2
    FC_node_score_M1toM2 = {}
    for key, value in logFC_mRNA_Node_M1toM2.items():
        raw_value = float(value)
        if raw_value <= 0 :
            FC_node_score_M1toM2[key] = -1 # This is Bad
        else:
            FC_node_score_M1toM2[key] = 1 # This is Good

    # Scoring the edges #########################################
    '''
    This is where the magic happens
    ''' 

    print("PKHmaybe just generalize to 'scores' as we may use different data later")  
    edge_mRNA_score_M2toM12 = []
    edge_mRNA_score_M12toM2 = []
    edge_mRNA_score_M2toM1 = []
    edge_mRNA_score_M1toM2 = []
    
    for n in np.arange(num_of_edges):
        feature = networkdata['edge_features'][n]
        startnode = networkdata['start_nodes'][n]
        endnode = networkdata['end_nodes'][n]
        
        try: # by doing so, we are skipping some ligands that are not registered in mRNA seq **
            startM2  = M2_node_score[startnode]
            startM1  = M1_node_score[startnode]
            startM12 = M1M2_node_score[startnode]

        except: 
            startM2 = 0
            startM1 = 0 
            startM12 = 0 

        try: # by doing so, we are skipping some ligands that are not registered in mRNA seq **
            endM2    = M2_node_score[endnode]
            endM1    = M1_node_score[endnode]
            endM12   = M1M2_node_score[endnode]

        except: 
            #pass
            endM2 = 0
            endM1 = 0 
            endM12 = 0
        
        try: # by doing so, we are skipping some ligands that are not registered in mRNA seq **
            startFC_M2toM12 = FC_node_score_M2toM12[startnode]
            startFC_M12toM2 = FC_node_score_M12toM2[startnode]
            startFC_M2toM1  = FC_node_score_M2toM1[startnode]
            startFC_M1toM2  = FC_node_score_M1toM2[startnode]
            
        except: 
            startFC_M2toM12 = 0
            startFC_M12toM2 = 0
            startFC_M1toM2 = 0
            startFC_M2toM1 = 0 

        try: # by doing so, we are skipping some ligands that are not registered in mRNA seq **
            endFC_M2toM12   = FC_node_score_M2toM12[endnode]
            endFC_M12toM2   = FC_node_score_M12toM2[endnode]
            endFC_M2toM1    = FC_node_score_M2toM1[endnode]
            endFC_M1toM2    = FC_node_score_M1toM2[endnode]
            
        except: 
            endFC_M2toM12 = 0
            endFC_M12toM2 = 0
            endFC_M1toM2 = 0
            endFC_M2toM1 = 0

        if feature == 'up-regulates':
            score_M2toM12 = math.exp(-((startM2 + endM2)/2+startFC_M2toM12+endFC_M2toM12))
            score_M12toM2 = math.exp(-((startM12 + endM12)/2+startFC_M12toM2+endFC_M12toM2))
            score_M2toM1  = math.exp(-((startM2 + endM2)/2+startFC_M2toM1+endFC_M2toM1))
            score_M1toM2  = math.exp(-((startM1 + endM1)/2+startFC_M1toM2+endFC_M1toM2))
        elif feature == 'down-regulates':
            #print("Here") 
            score_M2toM12 = math.exp(((startM2 + endM2)/2+startFC_M2toM12+endFC_M2toM12)) + 100 # I could add 1000 but let's see how it goes
            score_M12toM2 = math.exp(((startM12 + endM12)/2+startFC_M12toM2+endFC_M12toM2)) + 100 # I could add 1000 but let's see how it goes
            score_M2toM1  = math.exp(((startM2 + endM2)/2+startFC_M2toM1+endFC_M2toM1)) + 100 # I could add 1000 but let's see how it goes
            score_M1toM2  = math.exp(((startM1 + endM1)/2+startFC_M1toM2+endFC_M1toM2)) + 100 # I could add 1000 but let's see how it goes

        edge_mRNA_score_M2toM12.append(score_M2toM12)
        edge_mRNA_score_M12toM2.append(score_M12toM2)
        edge_mRNA_score_M2toM1.append(score_M2toM1)
        edge_mRNA_score_M1toM2.append(score_M1toM2)

    networkDict['edge_mRNA_score_M2toM12']  = edge_mRNA_score_M2toM12
    networkDict['edge_mRNA_score_M12toM2']  = edge_mRNA_score_M12toM2
    networkDict['edge_mRNA_score_M2toM1']  = edge_mRNA_score_M2toM1
    networkDict['edge_mRNA_score_M1toM2'] = edge_mRNA_score_M1toM2
    
    print("BC DEBUG two keys are same")
    for score in ["M2toM12","M12toM2","M2toM1","M1toM2"]:
        print(score," tot ", np.sum( networkDict['edge_mRNA_score_'+score]  ))

    #return unique_all, networkdata, edge_mRNA_score_M2toM12, edge_mRNA_score_M12toM2, edge_mRNA_score_M2toM1, edge_mRNA_score_M1toM2
    return networkDict

def networkAnalaysis(unique_all, networkdata, 
                     figure_file_name, receptorlistFileName, destination,
                     listofNodes_path=True,
                     edgeScores=None,
                     display=True):
    """
    This function looks like it builds the networkx graphs.
    Two graphs are created; one with mrna-derived weights, another is unweighted
    A receptor list is also provided; an optimal path from each member of this list to the 'destination' is identified
    This is done for both graph types
    """
    ## Drawing part 
    if display and edgeScores is not None: 
      from graphviz import Digraph  
      G2 = Digraph('unix', filename='fullpicture',format='png',
              node_attr={'shape':'box',
                         'color': 'blue:purple', 
                         'style': 'filled',
                         'fontcolor':'white'},
              edge_attr={'color': 'red'})
      G3 = Digraph('unix', filename='fullpicture',format='png',
              node_attr={'shape':'box',
                         'color': 'blue:purple', 
                         'style': 'filled',
                         'fontcolor':'white'},
              edge_attr={'color': 'red'}) 
      G5 = Digraph('unix', filename='fullpicture',format='png',
              node_attr={'shape':'box',
                         'color': 'blue:purple', 
                         'style': 'filled',
                         'fontcolor':'white'},
              edge_attr={'color': 'red'})

      for node in unique_all:
        G2.node(node)
        G3.node(node)
        G5.node(node)
        
  
      for n in np.arange(len(networkdata['start_nodes'])):
        weight = edgeScores[n]
        G2.edge(networkdata['start_nodes'][n],networkdata['end_nodes'][n],label=str(weight))
        G3.edge(networkdata['start_nodes'][n],networkdata['end_nodes'][n])
        if weight <= 1:
          name='up-regulated'
        elif weight > 1:
          name='down-regulated'
        G5.edge(networkdata['start_nodes'][n],networkdata['end_nodes'][n],label=name)
  
      
      G3.view(figure_file_name+'_no_weight')
      G5.view(figure_file_name+'_up_down')
      G2.view(figure_file_name)

    
    #####    
    ## Actual Network Analysis
    ##### 
    print("Net analysis")
    G = nx.DiGraph()
    
    for node in unique_all:
      G.add_node(node)
    
    
    G.nodes()
    daSum=0.
    if edgeScores is not None:
        for n in np.arange(len(networkdata['start_nodes'])):       
          weight = float( edgeScores[n] )
          G.add_edge(networkdata['start_nodes'][n],networkdata['end_nodes'][n],weight=weight) 
          #print(networkdata['start_nodes'][n],networkdata['end_nodes'][n],weight)
          daSum+=weight
 
    else:
        for n in np.arange(len(networkdata['start_nodes'])):
          G.add_edge(networkdata['start_nodes'][n],networkdata['end_nodes'][n])
   
    Receptor_info = textreader(receptorlistFileName)
    Receptor_name = Receptor_info['Receptors']
    Receptor_feature = Receptor_info['Pro/Anti']
    Outcome = destination
    
    

    Scorelist = []
    ReceptorList = []
    ReceptorFeatureList = []

    PathwayNodes = {}
   

    i = 0
    for R in Receptor_name:
      #print(R)
      try:
        Path = nx.dijkstra_path(G,R,Outcome)
        Score = nx.dijkstra_path_length(G,R,Outcome)
        #print(R,Score)
        
        Scorelist.append(Score)
        ReceptorList.append(R)
        ReceptorFeatureList.append(Receptor_feature[i])
        
        PathwayNodes[R] = Path
        if listofNodes_path:
          print('The shortest path from ',R,
                ' (Upstream Receptor) to ',Outcome,' in a weighted Pathway Network: \n', Path,
                '\n with score of ',Score,'\n')
      except:
        pass
      i += 1

    collected_data = {
            'g': G, # my graph 
            'Receptors': ReceptorList, 'Pro/Anti': ReceptorFeatureList, 'Score': Scorelist}      
    


    return collected_data, PathwayNodes # , collected_data2, PathwayNodes2

def expression_search(name_node,
                      data_mrna,
                      prot_data,
                      name_from_prot):
    
  prot_exp = 'N/A'   
  if prot_data is not None:
      for name_prot in name_from_prot:
        if name_node.lower() == name_prot.lower():
          element_prot = prot_data[name_prot]
      
      try:
        if element_prot:
          prot_exp = element_prot
      except:
        1
           
  M1 = 'N/A'
  M2 = 'N/A'
  M1M2 = 'N/A'
  logFCofM1M2overM2 = 'N/A' 
  if data_mrna is not None:
      for name_mrna in data_mrna['mgi_symbol']:
        if name_node.lower() == name_mrna.lower():
          n = data_mrna['mgi_symbol'].index(name_mrna)
          expM2 = float(data_mrna['M2'][n])
          expM1 = float(data_mrna['M1'][n])
          expM1M2 = float(data_mrna['M1M2\n'][n])
          compFC = math.log2(expM1M2/expM2)
      
      try:
        if n:
          M1 = expM1
          M2 = expM2 
          M1M2 = expM1M2 
          logFCofM1M2overM2 = compFC 
      except:
        1

  return prot_exp, M1, M2, M1M2, logFCofM1M2overM2

def TableMaker(M1target,
               M2target,
               M1control,
               M2control):
  
  M1M2Scoretarget = []
  M1M2Scorecontrol = []
  M1M2Scoreref = []
  Prediction = []
  for i in np.arange(len(M1target['Score'])):
    newtarget = float(M1target['Score'][i])/float(M2target['Score'][i])
    newcontrol = float(M1control['Score'][i])/float(M2control['Score'][i])
    newref = float(M1target['Score'][i])/float(M2control['Score'][i])
    M1M2Scoretarget.append(newtarget)
    M1M2Scorecontrol.append(newcontrol)
    M1M2Scoreref.append(newref)
    if newref < 10:
      Prediction.append('Pro-Inflammatory')
    else:
      Prediction.append('Anti-Inflammatory')

  M1M2totalScore = {'Receptor': M1target['Receptors'],
                  'Pro/Anti': M1target['Pro/Anti'], 
                  'M1_target': M1target['Score'],
                  'M1_control': M1control['Score'],
                  'M2_target': M2target['Score'],
                  'M2_control': M2control['Score'], 
                  'M1_target/M2_target': M1M2Scoretarget,
                  'M1_control/M2_control': M1M2Scorecontrol,
                  'M1_target/M2_control': M1M2Scoreref,
                  'Prediction': Prediction}

  return M1M2totalScore

def TableMaker2(target_dataset,
                control_dataset,
                unweighted_dataset,
                listofNodes_target,
                listofNodes_control,
                listofNodes_unweighted
                ):
  Receptors = target_dataset['Receptors']
  UnweightedScore = unweighted_dataset['Score']
  TargetScore = target_dataset['Score']
  ControlScore = control_dataset['Score']
  list1 = []
  list2 = []
  list3 = []
  newset={}
  newset['Receptor'] = Receptors
  newset['Pro/Anti'] = target_dataset['Pro/Anti']
  newset['Unweighted Score'] = UnweightedScore
  newset['Target Score (test group)'] = TargetScore
  newset['Control Score (reference)'] = ControlScore
  for name1 in Receptors:
    node1 = ''
    node2 = ''
    node3 = ''
    for n1 in listofNodes_target[name1]:
      if name1 == n1:
        node1 += str(n1)
      else:
        node1 += '->'+str(n1)
    for n2 in listofNodes_control[name1]:
      if name1 == n2:
        node2 += str(n2)
      else:
        node2 += '->'+str(n2)
    for n3 in listofNodes_unweighted[name1]:
      if name1 == n3:
        node3 += str(n3)
      else:
        node3 += '->'+str(n3)

    list1.append(node1)
    list2.append(node2)
    list3.append(node3)
    
  newset['Target Path'] = list1 
  newset['Control Path'] = list2 
  newset['Unweighted Path'] = list3 


  return newset 

def scorebymatching(path_M1):
  receptor_list = textreader('receptorlist')
  data = {}
  for receptor in receptor_list['Receptors']:
    data[receptor] = {}
    
    List = path_M1[receptor]
    nodes = List.keys()
    mAb = {}
    phos = {}
    M1mRNA = {}
    M2mRNA = {}
    M1M2mRNA = {}
    logFC = {}
    for node in nodes:
      entries = List[node].keys()
      for entry in entries:
        if entry == 'mAb(From M2 to M1: M1/M2)':
          mAb[node] = (List[node][entry])
        elif entry == 'Phospho(From M2 to M1: M1/M2)':
          phos[node]=(List[node][entry])
        elif entry == 'M1_mRNA':
          M1mRNA[node]=(List[node][entry])
        elif entry == 'M2_mRNA':
          M2mRNA[node]=(List[node][entry])
        elif entry == 'logFC(M1M2/M2)':
          M1M2mRNA[node]=(List[node][entry])
        else:
          logFC[node]=(List[node][entry])

    total_case_phos = 0
    match_case_phos = 0
    aval_case_phos = []
    matched_node_phos = []

    total_case_ab = 0
    match_case_ab = 0
    aval_case_ab = []
    matched_node_ab = []

    for node in nodes:
      value1 = phos[node]
      value2 = mAb[node]
      value3 = logFC[node]
      
      if value1 != 'N/A' and value3 != 'N/A':
        total_case_phos += 1
        aval_case_phos.append(node)
        if (value1 > 1 and value3 > 0) or (value1 < 1 and value3 < 0):
          match_case_phos += 1
          matched_node_phos.append(node)
        

      if value2 != 'N/A' and value3 != 'N/A':
        total_case_ab += 1
        aval_case_ab.append(node)
        if (value2 > 1 and value3 > 0) or (value2 < 1 and value3 < 0):
          match_case_ab += 1
          matched_node_ab.append(node)
    
    if total_case_phos == 0:
      data[receptor]['Available Nodes in mRNA and Phos'] = 'No Match Found'
      data[receptor]['Agreed between mRNA and Phos'] = 'No Match Found'
      data[receptor]['Match % between mRNA and Phos'] = 'No Match Found' 
    else:
      data[receptor]['Available Nodes in mRNA and Phos'] = aval_case_phos
      data[receptor]['Agreed between mRNA and Phos'] = matched_node_phos
      data[receptor]['Match % between mRNA and Phos'] = match_case_phos/total_case_phos*100
    
    if total_case_ab == 0:
      data[receptor]['Available Nodes in mRNA and mAb'] = 'No Match Found'
      data[receptor]['Agreed between mRNA and mAb'] = 'No Match Found'
      data[receptor]['Match % between mRNA and mAb'] = 'No Match Found'
    else:
      data[receptor]['Available Nodes in mRNA and mAb'] = aval_case_ab
      data[receptor]['Agreed between mRNA and mAb'] = matched_node_ab
      data[receptor]['Match % between mRNA and mAb'] = match_case_ab/total_case_ab*100

  return data


def WeightEdges(featureList):
    """
    create weights for 'up-regulates'-->1, 'down-regulates'-->100
    """
    ups = [ 0 if e == 'up-regulates' else 1 for e in featureList]
    ups = np.asarray(ups,float)
    downs = [ 100 if e == "down-regulates" else 0 for e in featureList]
    downs = np.asarray(downs,float)
    weights = ups + downs
    if 1 in weights:
        print("Some edge features were unassigned ")
    weights = np.asarray(weights,dtype=float)
    #print("np ",np.sum(weights))    
    return weights